public class Task {

    public String title;
    public String description;

    public Task(String title, String description){
        this.title = title;
        this.description = description;
    }

}
