import java.io.*;
import java.util.ArrayList;

public class Program {


    public static void main(String[] args) throws IOException {
        String filename = "file.ser";
        if (args[0].equals("-createUser")) {
            boolean usernameValid = true;

            ObjectInputStream ins = null;
            ArrayList<User> v = null;

            try {
                ins = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(filename))));

                try {
                    v = (ArrayList<User>) ins.readObject();

                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }

                for (User user : v) {
                    if (user.UserName.equals(args[3]))
                        usernameValid = false;
                    break;
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();


                if (usernameValid) {
                    ArrayList<User> woi = new ArrayList<>();
                    try {
                        FileOutputStream fop = new FileOutputStream(filename);
                        ObjectOutputStream oos = new ObjectOutputStream(fop);
                        woi.add(new User(args[1], args[2], args[3]));
                        oos.writeObject(woi);

                    } catch (IOException e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

        //callMyMethod();
        else if (args[0].equals("-showAllUsers")) {
            ObjectInputStream ins = null;
            ArrayList<User> v = null;

            try {
                ins = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(filename))));

                try {
                    v = (ArrayList<User>) ins.readObject();

                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }

                for (User user : v) {
                    System.out.println("UserName" + user.UserName);
                    System.out.println("FirstName" + user.FirstName);
                    System.out.println("LastName" + user.LastName);
                }


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (ins != null) {
                    try {
                        ins.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (args[0].equals("-addTask")) {
            boolean userFound = false;

            ObjectInputStream ins = null;
            ArrayList<User> v = null;

            try {
                ins = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(filename))));

                try {
                    v = (ArrayList<User>) ins.readObject();

                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }

                for (User user : v) {
                    if (user.UserName.equals(args[1])) {
                        user.taskList.add(new Task(args[2], args[3]));
                    }

                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();

            }
        } else if (args[0].equals("-showTasks")) {
            ObjectInputStream ins = null;
            ArrayList<User> v = null;

            try {
                ins = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(filename))));

                try {
                    v = (ArrayList<User>) ins.readObject();

                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                }

                for (User user : v) {
                    if (user.UserName.equals(args[1])) {
                        for (int i = 0; i < user.taskList.size(); i++) {
                            System.out.println(user.taskList.get(i).title);
                            System.out.println(user.taskList.get(i).description);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}